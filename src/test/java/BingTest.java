
import org.junit.Assert;;
import org.openqa.selenium.*;

import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;


public class BingTest {
        public static WebDriver myWebDriver;
        public static final String bingUrl = "https://www.bing.com/";
        public static final String googleUrl = "https://www.google.com";

        @Test
        public void navigateToBing() {
            System.setProperty("webdriver.chrome.driver", "C:\\Users\\promil\\Desktop\\Telerik Alpha\\QA\\chromedriver.exe");
            myWebDriver = new ChromeDriver();
            myWebDriver.manage().window().maximize();
            myWebDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
            myWebDriver.navigate().to(bingUrl);

                WebElement inputSearch = myWebDriver.findElement(By.xpath("//input[@id='sb_form_q']"));
                inputSearch.sendKeys("Telerik Academy Alpha");
                inputSearch.sendKeys(Keys.RETURN);
                String expectedTitle = "IT Career Start in 6 Months - Telerik Academy Alpha";
                WebElement firstElement = myWebDriver
                    .findElement(By.className("b_topTitle"))
                    // get first child (the link)
                    .findElements(By.xpath("./child::*"))
                    .get(0);


            String title = firstElement.getText();
            Assert.assertTrue(title.equalsIgnoreCase(expectedTitle));
            myWebDriver.quit();
        }

        @Test
        public void navigateToGoogleChrome(){
            System.setProperty("webdriver.chrome.driver", "C:\\Users\\promil\\Desktop\\Telerik Alpha\\QA\\chromedriver.exe");
            myWebDriver = new ChromeDriver();
            myWebDriver.manage().window().maximize();
            myWebDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
            myWebDriver.navigate().to(googleUrl);

            myWebDriver.findElement(By.xpath("//button[@id='L2AGLb']")).click();
            WebElement inputSearch = myWebDriver.findElement(By.xpath("//input[@class='gLFyf gsfi']"));
            inputSearch.sendKeys("Telerik Academy Alpha");
            inputSearch.sendKeys(Keys.RETURN);
            String expectedTitle = "IT Career Start in 6 Months - Telerik Academy Alpha";
            WebElement firstElement = myWebDriver
                    .findElements(By.xpath("//h3[@class='LC20lb MBeuO DKV0Md']"))
                    .get(0);


            String title = firstElement.getText();
            Assert.assertTrue(title.equalsIgnoreCase(expectedTitle));
            myWebDriver.quit();
        }
        }




